package com.backend.Services;

import org.springframework.stereotype.Service;

import com.backend.Entities.AccompagnateurEntity;
import com.backend.Services.common.ReadService;
import com.backend.dto.AccompagnateurDTO;

@Service
public class AccompagnateurService extends ReadService<AccompagnateurDTO, AccompagnateurEntity>{

}
